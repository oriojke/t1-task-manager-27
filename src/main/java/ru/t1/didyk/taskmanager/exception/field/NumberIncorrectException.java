package ru.t1.didyk.taskmanager.exception.field;

import org.jetbrains.annotations.NotNull;

public final class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Incorrect number.");
    }

    public NumberIncorrectException(@NotNull final String value, @NotNull Throwable cause) {
        super("Error! Value \"" + value + "\" is incorrect.");
    }

}
