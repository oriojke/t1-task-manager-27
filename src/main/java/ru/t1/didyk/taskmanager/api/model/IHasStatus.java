package ru.t1.didyk.taskmanager.api.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
